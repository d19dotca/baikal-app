FROM cloudron/base:0.9.0
MAINTAINER Johannes Zellner <johannes@nebulon.de>

RUN mkdir -p /app/code
WORKDIR /app/code/baikal

RUN cd /tmp && \
    wget https://github.com/fruux/Baikal/releases/download/0.4.6/baikal-0.4.6.zip && \
    unzip baikal-0.4.6.zip -d /app/code && \
    rm baikal-0.4.6.zip

WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache2-baikal.conf /etc/apache2/sites-available/baikal.conf
RUN ln -sf /etc/apache2/sites-available/baikal.conf /etc/apache2/sites-enabled/baikal.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN apt-get update && apt-get install -y php libapache2-mod-php && rm -r /var/cache/apt /var/lib/apt/lists

RUN a2enmod rewrite
RUN sed -e 's/upload_max_filesize = .*/upload_max_filesize = 100M/' \
        -e 's,;session.save_path.*,session.save_path = "/run/baikal/sessions",' \
        -i /etc/php/7.0/apache2/php.ini

RUN mkdir -p /run/baikal/sessions

RUN rm -rf /app/code/baikal/Specific && ln -sf /app/data/Specific /app/code/baikal/Specific

ADD config.php.template config.system.php.template /app/code/

RUN chown -R www-data.www-data /app/code /run/baikal

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
